import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.util.ResourceBundle;

public class View implements Initializable {

    @FXML
    public ComboBox alphabet;

    @FXML
    public Spinner numberOfSentence;

    @FXML
    public TextArea result;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alphabet.getItems().add("Georgian");
        alphabet.getItems().add("English");
    }

    public void generate(){
        String al = (String) alphabet.getValue();
        int n = (int) numberOfSentence.getValue();
        result.setText("My Result"+" "+n+" "+al);
    }
}
