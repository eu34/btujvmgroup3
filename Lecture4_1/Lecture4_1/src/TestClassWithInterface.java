public class TestClassWithInterface implements TestInterface, TestInterface3 {
    public void m7(){

    }

    @Override
    public void m1() {

    }

    @Override
    public int m2(int x, int y) {
        return 0;
    }

    @Override
    public String m3() {
        return null;
    }

    @Override
    public void m22() {

    }

    @Override
    public int m34() {
        return 0;
    }
}
