import java.io.*;

public class Files {
    public Files() {

    }

    public void createFile(){
        try {
            FileWriter writer = new FileWriter("file.txt", true);
            writer.write("Java");
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void createFile2(){
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("c:/jvm/file3.txt"));
            writer.write("Hello Java");
            writer.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void readFile(){
        try {
            FileReader reader = new FileReader("file.txt");
            System.out.println(reader.read());
            reader.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
