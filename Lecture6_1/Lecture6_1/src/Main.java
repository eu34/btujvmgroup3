public class Main {
    public static void main(String[] args) {
        Thread1 t1 = new Thread1();
//        t1.m1();
//        t1.run();
        t1.start();

        Thread2 t2 = new Thread2();
//        t2.run();

        Thread t = new Thread(t2);
        t.setName("My Thread 2");
        t.setPriority(9);
        t.start();

        int N = 10;
        System.out.println(Thread.currentThread().getName());
        for(int i=0; i<=N; i++){
            System.out.println("Main -> "+i);
        }



        System.out.println("Main Priority - "+Thread.currentThread().getPriority());
    }
}